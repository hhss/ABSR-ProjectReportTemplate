% !TeX program = xelatex
% !TeX encoding = UTF-8

\ProvidesClass{ABSR-PR006}[2017/2/13, V0.1, ABSR Project Template, Project Report: Project Proposal]%
\LoadClass[a4paper, twoside]{article}%


% 中文字体设定
\RequirePackage[CJKmath=true]{xeCJK}%
\setCJKmainfont{SimSun}%
\setCJKfamilyfont{song}{SimSun}%
\newcommand*{\song}{\CJKfamily{song}}%
\setCJKfamilyfont{hei}{SimHei}%
\newcommand*{\hei}{\CJKfamily{SimHei}}%
\setCJKfamilyfont{kai}{KaiTi}%
\newcommand*{\kai}{\CJKfamily{kai}}%
\setCJKfamilyfont{you}{YouYuan}%
\newcommand*{\you}{\CJKfamily{you}}%
\setCJKfamilyfont{fsong}{FangSong}%
\newcommand*{\fsong}{\CJKfamily{fsong}}%
\setCJKfamilyfont{li}{LiSu}%
\newcommand*{\li}{\CJKfamily{li}}%

% 数学字体设定
\RequirePackage{amsmath,amssymb,amsfonts}%
\RequirePackage{pifont}%

% 列表设定
\RequirePackage{enumerate}
\RequirePackage{enumitem}
\newlist{renumerate}{enumerate}{3}
\setlist[renumerate]{label=\protect\ding{\value*},start=172}


% 插图设定
\RequirePackage{graphics}%
\RequirePackage{graphicx}%
\RequirePackage[rgb]{xcolor}%
\RequirePackage{subfigure}%
\RequirePackage{caption}%
\RequirePackage{float}%
\RequirePackage{epsfig}%

% 表格设定
\RequirePackage{multirow}%
\RequirePackage{makecell}%
\RequirePackage{array}%
\RequirePackage{booktabs}%
\RequirePackage{longtable}%
\RequirePackage{tabularx}%
\def\tabularxcolumn#1{m{#1}}

% 超链接设定
\RequirePackage{url}%
\RequirePackage{cite}%
\RequirePackage[colorlinks,linkcolor=blue]{hyperref}%

% 页面布局
\RequirePackage{fancyhdr}%
\RequirePackage{geometry}%
\geometry{left=3.18cm,right=3.18cm,top=2.54cm,bottom=2.54cm}%

% 高级自定义
\RequirePackage{environ}%




% 专有命令
\newcommand{\Org}[1]{\def\Organization{#1}}%
\newcommand{\Depart}[1]{\def\Department{#1}}%
\newcommand{\Group}[1]{\def\ReseachGroup{#1}}%
\newcommand{\ProjNum}[1]{\def\ProjectNumber{#1}}%
\newcommand{\ProjName}[1]{\def\ProjectName{#1}}%
\newcommand{\ProjMan}[1]{\def\ProjectManager{#1}}%

\newcommand{\makeheader}{
	\title{{\hei\ProjectName} \\ {\li 结构设计报告}}
	\author{项目负责人：{\kai\ProjectManager}}
	\date{}
	\maketitle
	
	\thispagestyle{fancy}
	\lfoot{\Organization}
	\rfoot{\Department}
	\fancyhead[CE]{项目名称：\ProjectName}
	\fancyhead[LE]{}
	\fancyhead[RE]{}
	\fancyhead[RO]{PR006-结构设计报告}
	\fancyhead[LO]{项目编号：\ProjectNumber}
	

	\pagenumbering{arabic}
	\pagestyle{fancy}
}






% 文档修改历史记录
\makeatletter 
\newcommand{\edithistory}{\def\@body{}\@stepone}
\newcommand\@stepone{\@ifnextchar\stophistory{}{\@steptwo}}
\newcommand\@steptwo[4]%
   {\expandafter\def\expandafter\@body\expandafter{\@body #1 & #2 & #3 & #4 \\ \hline}%
    \@stepone
   }
\newcommand\stophistory
   {\vspace{-1em}
   \noindent
   \begin{center}
   \begin{tabularx}{.95\textwidth}{|>{\hsize=.15\hsize}X|>{\hsize=.6\hsize}X|>{\hsize=.1\hsize}X|>{\hsize=.13\hsize}X|} \hline
    {\you 文档版本} & {\you 修改内容} & {\you 修改人} & {\you 修改日期}\\ \hline
    \@body
    \end{tabularx}%
    \end{center}
   }
\makeatother



% 项目人员照片及简介
\makeatletter
\NewEnviron{members}{%
  %\par\noindent
  \toks@={}%
  \expandafter\start@member\BODY\start@member
  \edef\BODY{\noexpand\begin{tabularx}{.94\textwidth}{|>{\hsize=.115\textwidth}X|>{\hsize=.7645\textwidth}X|}
  \noexpand\hline
  \the\toks@
  \noexpand\end{tabularx}}%
  \BODY
}
\newcommand\start@member{\@ifnextchar\start@member{\@gobble}{\@member}}
\newcommand{\@member}[2]{%
  \toks@=\expandafter{\the\toks@ \raisebox{-0.6\height}{\fcolorbox{white}{white}{\includegraphics[width=.1\textwidth]{#1}}} & #2 \\ \hline}%
  \start@member
}
\makeatother


% 文档审阅历史记录
\makeatletter 
\newcommand{\proofread}{\def\@body{}\@proofstepone}
\newcommand\@proofstepone{\@ifnextchar\stopproof{}{\@proofsteptwo}}
\newcommand\@proofsteptwo[4]%
   {\expandafter\def\expandafter\@body\expandafter{\@body #1 & #2 & #3 & #4 \\ \hline}%
    \@proofstepone
   }
\newcommand\stopproof
   {\noindent
   \vfill
   \begin{center}
   \begin{tabularx}{.95\textwidth}{|>{\hsize=.15\hsize}X|>{\hsize=.65\hsize}X|>{\hsize=.1\hsize}X|>{\hsize=.13\hsize}X|} \hline
    {\you 审阅版本} & {\you 审阅意见} & {\you 审阅人} & {\you 审阅日期}\\ \hline
    \@body
    \end{tabularx}%
    \end{center}
    \vfill
   }
\makeatother
